module Main exposing (..)

import Browser
import Dict
import Html exposing (..)
import Html.Attributes exposing (style)
import Html.Events exposing (onClick, onInput)
import Set
import Words exposing (words)


type CellClassification
    = CorrectPosition
    | IncorrectPosition
    | NoMatch


type alias Cell =
    { char : Char
    , classification : CellClassification
    }


type alias Model =
    { candidates : Set.Set String
    , initialCandidates : Set.Set String
    , results : List (List Cell)
    , currentCandidate : Maybe (List Cell)
    }



-- TODO select candidate based on information entropy


bestCandidate : Set.Set String -> Maybe String
bestCandidate candidates =
    candidates
        |> Set.toList
        |> List.head


charInCorrectPosition : String -> Int -> Char -> Bool
charInCorrectPosition candidate charPos char =
    String.fromChar char == String.slice charPos (charPos + 1) candidate


applyConstraints : Set.Set String -> Constraints -> Set.Set String
applyConstraints candidates constraints =
    let
        passes candidate =
            let
                chars =
                    candidate
                        |> String.toList
                        |> Set.fromList
            in
            Set.isEmpty (Set.intersect chars constraints.forbiddenChars)
                && (constraints.correctPositionChars
                        |> Dict.toList
                        |> List.all (\posChar -> charInCorrectPosition candidate (Tuple.first posChar) (Tuple.second posChar))
                   )
                && (constraints.incorrectPositionChars
                        |> Dict.toList
                        |> List.all
                            (\posChar ->
                                not (charInCorrectPosition candidate (Tuple.first posChar) (Tuple.second posChar))
                                    && String.contains (String.fromChar (Tuple.second posChar)) candidate
                            )
                   )
    in
    Set.filter passes candidates


prepareCandidate : Maybe String -> Maybe (List Cell)
prepareCandidate candidate =
    case candidate of
        Nothing ->
            Nothing

        Just c ->
            c
                |> String.toList
                |> List.map (\ch -> { char = ch, classification = NoMatch })
                |> Just


init : Model
init =
    let
        initialCandidates =
            Set.fromList words
    in
    { initialCandidates = initialCandidates
    , candidates = initialCandidates
    , results = []
    , currentCandidate =
        initialCandidates
            |> bestCandidate
            |> prepareCandidate
    }


type Msg
    = ComputeNextCandidate
    | CycleClassification Int


nextClassification : CellClassification -> CellClassification
nextClassification classification =
    case classification of
        NoMatch ->
            CorrectPosition

        CorrectPosition ->
            IncorrectPosition

        IncorrectPosition ->
            NoMatch


cycleCellClassification : Model -> Int -> Model
cycleCellClassification model cellPos =
    let
        currentCandidate =
            model.currentCandidate
    in
    case currentCandidate of
        Nothing ->
            model

        Just candidate ->
            { model
                | currentCandidate =
                    candidate
                        |> List.indexedMap
                            (\cellPosIter cell ->
                                if cellPos == cellPosIter then
                                    { cell
                                        | classification = nextClassification cell.classification
                                    }

                                else
                                    cell
                            )
                        |> Just
            }


type alias Constraints =
    { forbiddenChars : Set.Set Char
    , correctPositionChars : Dict.Dict Int Char
    , incorrectPositionChars : Dict.Dict Int Char
    }


constraintsFromResult : List Cell -> Constraints
constraintsFromResult result =
    let
        noMatchCells =
            result
                |> List.filter (\cell -> cell.classification == NoMatch)
                |> List.map .char
                |> Set.fromList

        correctPositionCells =
            result
                |> List.filter (\cell -> cell.classification == CorrectPosition)
                |> List.map .char
                |> Set.fromList

        incorrectPositionCells =
            result
                |> List.filter (\cell -> cell.classification == IncorrectPosition)
                |> List.map .char
                |> Set.fromList
    in
    { forbiddenChars =
        Set.diff noMatchCells (Set.union correctPositionCells incorrectPositionCells)
    , correctPositionChars =
        result
            |> List.indexedMap (\cellPos cell -> ( cellPos, cell ))
            |> List.filter (\arg -> (Tuple.second arg).classification == CorrectPosition)
            |> List.map (\arg -> ( Tuple.first arg, (Tuple.second arg).char ))
            |> Dict.fromList
    , incorrectPositionChars =
        result
            |> List.indexedMap (\cellPos cell -> ( cellPos, cell ))
            |> List.filter (\arg -> (Tuple.second arg).classification == IncorrectPosition)
            |> List.map (\arg -> ( Tuple.first arg, (Tuple.second arg).char ))
            |> Dict.fromList
    }


mergeWithPreviousResult : Maybe (List Cell) -> Maybe (List Cell) -> Maybe (List Cell)
mergeWithPreviousResult prevResult candidate =
    let
        pickCell resultCell candidateCell =
            if resultCell.classification == CorrectPosition then
                resultCell

            else
                candidateCell
    in
    case ( prevResult, candidate ) of
        ( Nothing, Nothing ) ->
            Nothing

        ( Nothing, Just c ) ->
            Just c

        ( Just pr, Nothing ) ->
            Nothing

        ( Just pr, Just c ) ->
            Just (List.map2 pickCell pr c)


computeNextCandidate : Model -> Model
computeNextCandidate model =
    let
        result =
            model.currentCandidate

        currentCandidates =
            model.candidates

        constraints =
            model.currentCandidate
                |> Maybe.withDefault []
                |> constraintsFromResult

        nextCandidates =
            applyConstraints currentCandidates constraints
    in
    { model
        | candidates =
            nextCandidates
        , currentCandidate =
            nextCandidates
                |> bestCandidate
                |> prepareCandidate
                |> mergeWithPreviousResult result
        , results =
            case result of
                Nothing ->
                    model.results

                Just res ->
                    res :: model.results
    }


update : Msg -> Model -> Model
update msg model =
    case msg of
        ComputeNextCandidate ->
            computeNextCandidate model

        CycleClassification cellPos ->
            cycleCellClassification model cellPos


cellClassificationBackground : CellClassification -> String
cellClassificationBackground classification =
    case classification of
        NoMatch ->
            "lightgrey"

        CorrectPosition ->
            "mediumseagreen"

        IncorrectPosition ->
            "orange"


cellMarkingView : Int -> Cell -> Html Msg
cellMarkingView cellPos cell =
    div
        [ style "background-color" (cellClassificationBackground cell.classification)
        , style "height" "2em"
        , style "width" "2em"
        , style "flex-grow" "0"
        , style "flex-shrink" "1"
        , style "user-select" "none"
        , style "text-align" "center"
        , onClick (CycleClassification cellPos)
        ]
        [ text (String.fromChar cell.char) ]


candidateMarkingView : List Cell -> Html Msg
candidateMarkingView candidate =
    let
        candidateControl =
            candidate
                |> List.indexedMap cellMarkingView
                |> div [ style "display" "flex" ]
    in
    div
        []
        [ candidateControl
        , button [ onClick ComputeNextCandidate ] [ text "Next step" ]
        ]


view : Model -> Html Msg
view model =
    case model.currentCandidate of
        Nothing ->
            div [] [ text "no candidates" ]

        Just c ->
            candidateMarkingView c


main =
    Browser.sandbox
        { view = view
        , update = update
        , init = init
        }
